﻿using Microsoft.WindowsAPICodePack.Dialogs;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetecter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            /*
            using (var img = new IplImage(@"C:\Users\mizofumi\Desktop\lena.png"))
            {
                Cv.SetImageROI(img, new CvRect(200, 200, 180, 200));
                Cv.Not(img, img);
                Cv.ResetImageROI(img);
                using(new CvWindow(img))
                {
                    Cv.WaitKey();
                }
            }
            */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "mp4ファイル|*.mp4;*.mpeg4|tsファイル|*.ts;*.m2ts";
            if(dialog.ShowDialog() == DialogResult.OK)
            {
                //Console.WriteLine(dialog.FileName);
                if(dialog.FileName.Length != 0)
                {
                    string ext = Path.GetExtension(dialog.FileName);
                    if (ext.Equals(".ts") || ext.Equals(".m2ts"))
                    {
                        ConvertDialog c = new ConvertDialog();
                        c.setPath(dialog.FileName.ToString());
                        c.ShowDialog();
                    }
                    sourceFile.Text = dialog.FileName.ToString();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.Title = "保存先を指定してください";
            dialog.AllowNonFileSystemItems = false;
            dialog.IsFolderPicker = true;
            if(dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if(dialog.FileName.Length != 0)
                {
                    savePath.Text = dialog.FileName;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ConvertDialog c = new ConvertDialog();
            c.ShowDialog();
            /*
            FFmpeg f = new FFmpeg();
            f.convert("");
            */
        }
    }
}
