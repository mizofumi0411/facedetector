﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetecter
{
    public partial class ConvertDialog : Form
    {

        string sourcePath;
        private FFmpeg f;

        public ConvertDialog()
        {
            InitializeComponent();
            progressBar1.Maximum = 100;
            
        }

        internal void setPath(string v)
        {
            sourcePath = @v;
        }

        private void ConvertDialog_Load(object sender, EventArgs e)
        {
            f = new FFmpeg(this);
            f.MyProgressEvent += Progress;
            f.MyFinishedEvent += ProgressFinished;
        }

        private void ProgressFinished(FFmpeg.MyFinishedEventArgs e)
        {
            Console.WriteLine("ProgressFinished");
            this.Invoke((MethodInvoker)delegate ()
            {
                this.Close();
            });
            //throw new NotImplementedException();
        }

        private void Progress(FFmpeg.MyEventArgs e)
        {

            try
            {
                Console.WriteLine(e.Parcent + "%");

                progressBar1.Invoke((MethodInvoker)delegate () {
                    parcent.Text = e.Parcent + "%";
                    progressBar1.Value = e.Parcent;

                    if (e.Parcent >= 99)
                    {
                        this.Close();
                    }

                });
            }
            catch(Exception ee)
            {

            }
            
            

            //throw new NotImplementedException();
        }

        private void ConvertDialog_Shown(object sender, EventArgs e)
        {
            f.convert(sourcePath);
        }
    }
}
