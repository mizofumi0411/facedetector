﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceDetecter
{
    class FFmpeg
    {

        long dulation;
        string sourcePath;
        string outputPath;
        string binDir = Environment.CurrentDirectory + "\\bin\\";
        string ffmpeg = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + @"\bin\ffmpeg.exe";
        private ConvertDialog convertDialog;

        public Action<MyEventArgs> MyProgressFinished { get; internal set; }

        public FFmpeg(ConvertDialog convertDialog)
        {
            this.convertDialog = convertDialog;
        }

        public void convert(string path)
        {
            sourcePath = path;
            //path = @"C:\\Users\\mizof\\Documents\\Visual Studio 2015\\Projects\\FFmpegTest1\\FFmpegTest1\bin\\Debug\bin\\h.ts";
            string name = Path.GetFileName(path);
            string outDir = path.Replace(name, "");
            string outName = Path.GetFileNameWithoutExtension(path);
            string output = outDir+outName+".mp4";
            outputPath = @output;
            Console.WriteLine(outputPath);

            long d = getInfo(path);
            Console.WriteLine(d);
            run(path);
        }

        long getInfo(string path)
        {
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardError = true;
            p.ErrorDataReceived += info;
            p.StartInfo.FileName = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + @"\bin\ffmpeg.exe";
            p.StartInfo.RedirectStandardInput = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.Arguments = "-i " + "\"" + path + "\"";
            p.Start();
            p.BeginErrorReadLine();
            p.WaitForExit();
            p.Close();

            return dulation;
        }

        private void info(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null && e.Data.Contains("Duration"))
            {
                string[] da = e.Data.Split(null);
                string du = da[3].Replace(",", "").Replace(Environment.NewLine, "");
                this.dulation = getMillSec(du);
                Console.WriteLine(du);
            }
        }

        private long getMillSec(string time)
        {
            string[] t = time.Split(new char[] { ':', '.' });

            long t4 = long.Parse(t[3]);
            long t3 = long.Parse(t[2]) * 1000;
            long t2 = long.Parse(t[1]) * 1000 * 60;
            long t1 = long.Parse(t[0]) * 1000 * 60 * 60;

            return t4 + t3 + t2 + t1;
        }

        void run(string path)
        {
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardError = true;
            p.ErrorDataReceived += progress; ;
            p.StartInfo.FileName = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + @"\bin\ffmpeg.exe";
            p.StartInfo.RedirectStandardInput = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.Arguments = "-v quiet -stats -i " + "\"" + path + "\" " + "\"" + outputPath + "\"";

            p.EnableRaisingEvents = true;
            p.Exited += finished;
            p.Start();

            p.BeginErrorReadLine();
            

            //p.WaitForExit();
            p.Close();
        }

        private void finished(object sender, EventArgs e)
        {
            Console.WriteLine("finished");
            FinishedProgress();
            //throw new NotImplementedException();
        }

        private void progress(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null && e.Data.Contains("time"))
            {
                int start = e.Data.IndexOf("time");
                string time = e.Data.Substring(start + 5, 11);
                double parcent = (getMillSec(time) * 1.0 / dulation * 1.0) * 100;
                int par = Convert.ToInt32(parcent);

                UpdateProgress(par, sourcePath, outputPath);
                Console.WriteLine(getMillSec(time) + " " + dulation);
                Console.WriteLine(parcent+"%");
            }
        }


        //-----------------------------------
        //イベントハンドラのデリゲートを定義
        public delegate void MyEventHandler(MyEventArgs e);
        public delegate void MyFinishedHandler(MyFinishedEventArgs e);

        //-----------------------------------
        // Form1へ伝えるイベントハンドラを定義
        public event MyEventHandler MyProgressEvent;
        public event MyFinishedHandler MyFinishedEvent;

        // Form1へイベントを伝える関数を定義
        private void UpdateProgress(int Parcent, string SourcePath, string ConvertPath)
        {
            MyProgressEvent(new MyEventArgs(Parcent, SourcePath, ConvertPath));
        }
        
        private void FinishedProgress()
        {
            MyFinishedEvent(new MyFinishedEventArgs());
        }
        
        //-----------------------------------
        // 渡せるイベントデータ引数、EventArgsを継承したクラスを拡張
        public class MyEventArgs : EventArgs
        {
            private readonly int _Parcent;
            private readonly string _SourcePath;
            private readonly string _ConvertPath;

            public int Parcent
            {
                get
                {
                    return _Parcent;
                }
            }

            public string SourcePath
            {
                get
                {
                    return _SourcePath;
                }
            }

            public string ConvertPath
            {
                get
                {
                    return _ConvertPath;
                }
            }

            public MyEventArgs(int Parcent, string SourcePath, string ConvertPath)
            {
                _Parcent = Parcent;
                _SourcePath = SourcePath;
                _ConvertPath = ConvertPath;
            }
            
        }

        public class MyFinishedEventArgs : EventArgs
        {
        }
    }
}
